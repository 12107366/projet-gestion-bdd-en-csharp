﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniProjetPays
{
    public class Pays
    {
        
        public int CodeNum { get; set; }
        public string Alpha2 { get; set; }
        public string Alpha3 { get; set; }
        public string NomFR { get; set; }
        public string NomEn { get; set; }
        public string CapitaleFR { get; set; }
        public string CapitaleEN { get; set; }
    }
}
