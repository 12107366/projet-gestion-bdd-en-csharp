﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace MiniProjetPays
{

    public partial class Form1 : Form
    {  private static string chaineConnexionPays = "SERVER=localhost;DATABASE=Pays;UID=root;PASSWORD=root;";
        

        public Form1()
        {
            InitializeComponent();
            this.Text = "Projet Pays".ToString();
        }


        static void CreationBase()
        {
           
            try
            {
                using (MySqlConnection con = new MySqlConnection(chaineConnexionPays))
                {
                    con.Open();

                    String sql1 = "DROP DATABASE IF EXISTS Pays";
                    String sql2 = "CREATE DATABASE Pays";
                    try
                    {
                        using (MySqlCommand command = new MySqlCommand(sql1, con))
                        {
                            command.ExecuteNonQuery();
                            Console.WriteLine("Base supprimée !!!!");
                        }
                        using (MySqlCommand command = new MySqlCommand(sql2, con))
                        {
                            command.ExecuteNonQuery();
                            Console.WriteLine("Base créée !!!!");
                        }
                    }
                    catch { Console.WriteLine("Base non creee."); }

                   
                    con.Close(); 
                }
            }
            catch (Exception)
            { Console.WriteLine("PB de connexion!!!!!"); }

        }
        static void CreationTable()
        {
           
            String sql = "CREATE TABLE paysTable (CodeNum INT, Alpha2 VARCHAR(255) PRIMARY KEY , Alpha3 VARCHAR(255), NomFR VARCHAR(255), NomEn VARCHAR(255), CapitaleFR VARCHAR(255), CapitaleEN VARCHAR(255))";
            try
            {
                using (MySqlConnection con = new MySqlConnection(chaineConnexionPays))
                {
                    con.Open();
                    try
                    {
                        using (MySqlCommand command = new MySqlCommand(sql, con))
                        {
                            command.ExecuteNonQuery();
                        }

                    }
                    catch(MySqlException ex)
                    {
                        MessageBox.Show("Erreur lors de la création de la table : " + ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                        
                        con.Close(); 
                }
            }

            catch (Exception)
            { Console.WriteLine("PB de connexion!!!!!"); }
        }

        
        static void LoadBD()
        {
            
            List<string> list = new List<string>();
            try
            {
                string fichierCsv = Path.Combine(Application.StartupPath, "Data\\pays.csv");

                using (StreamReader reader = new StreamReader(fichierCsv)) { 
                    string line;

                    using (MySqlConnection con = new MySqlConnection(chaineConnexionPays))
                    {
                        con.Open();

                        while ((line = reader.ReadLine()) != null)
                        {
                            list.Add(line);
                            string[] data = line.Split(',');
                            string CodeNum = data[1];
                            string Alpha2 = data[2];
                            string Alpha3 = data[3];
                            string NomFR = data[4];
                            string NomEn = data[5];
                           

                            using (MySqlCommand command = new MySqlCommand("INSERT INTO paysTable (CodeNum, Alpha2, Alpha3, NomFR, NomEn, CapitaleFR, CapitaleEN) VALUES (@CodeNum, @Alpha2, @Alpha3, @NomFR, @NomEn, NULL, NULL)", con))
                            {
                                command.Parameters.AddWithValue("@CodeNum", CodeNum);
                                command.Parameters.AddWithValue("@Alpha2", Alpha2);
                                command.Parameters.AddWithValue("@Alpha3", Alpha3);
                                command.Parameters.AddWithValue("@NomFR", NomFR);
                                command.Parameters.AddWithValue("@NomEn", NomEn);
                                
                                command.ExecuteNonQuery();
                            }
                        }
                    }
                }
            }
            catch (IOException ex)
            {
                MessageBox.Show("Erreur d'entrée/sortie : " + ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erreur : " + ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        


        private void Form1_Load(object sender, EventArgs e)
        {
            CreationBase();
            CreationTable();
            LoadBD();
        }


        private void afficherLesPaysToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List<Pays> maListe = new List<Pays>();

            using (MySqlConnection con = new MySqlConnection(chaineConnexionPays))
            {
                con.Open();
                try
                {
                    String sql = "SELECT CodeNum, Alpha2, Alpha3, NomFR, NomEn, CapitaleFR, CapitaleEN FROM paysTable ORDER BY  CodeNum";

                    using (MySqlCommand command = new MySqlCommand(sql, con))
                    {
                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                maListe.Add(new Pays()
                                {
                                    CodeNum = reader.GetFieldValue<int>(0),
                                    Alpha2 = reader.GetFieldValue<string>(1),
                                    Alpha3 = reader.GetFieldValue<string>(2),
                                    NomFR = reader.GetFieldValue<string>(3),
                                    NomEn = reader.GetFieldValue<string>(4),
                                    CapitaleFR = reader["CapitaleFR"] is DBNull ? null : reader["CapitaleFR"].ToString(),
                                    CapitaleEN = reader["CapitaleEN"] is DBNull ? null : reader["CapitaleEN"].ToString()
                                });
                            }

                            dataGridView2.DataSource = maListe;
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Erreur d'affichage : " + ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                con.Close();
            }
        }



        public void rechercheAlpha2 (String S)
        {


            List<Pays> maListe = new List<Pays>();

            using (MySqlConnection con = new MySqlConnection(chaineConnexionPays))
            {
                con.Open();
                try
                {   
                    String sql = "SELECT * FROM paysTable WHERE Alpha2=@Alpha2";

                        using (MySqlCommand command = new MySqlCommand(sql, con))
                    {
                            command.Parameters.AddWithValue("@Alpha2", S);
                            using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            String s = "";
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                s += reader.GetName(i) + " ";
                            }
                            Console.WriteLine(s);
                            while (reader.Read())
                            {
                                for (int i = 0; i < reader.FieldCount; i++)
                                {
                                    var val = reader.GetValue(i);
                            
                                    Console.Write(val + " ");
                                }
                                maListe.Add(new Pays()
                                {

                                    CodeNum = reader.GetFieldValue<int>(0),
                                    Alpha2 = reader.GetFieldValue<string>(1),
                                    Alpha3 = reader.GetFieldValue<string>(2),
                                    NomFR = reader.GetFieldValue<string>(3),
                                    NomEn = reader.GetFieldValue<string>(4),

                                });
                            }

                            dataGridView2.DataSource = maListe;
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Erreur d'affichage : " + ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                con.Close(); 
            }

        }
        public void recherchePaysFR(String S)
        {
            List<Pays> maListe = new List<Pays>();
            using (MySqlConnection con = new MySqlConnection(chaineConnexionPays))
            {


                    con.Open();
                try
                {
                    String sql = "SELECT * FROM paysTable WHERE NomFR=@NomFR";

                    using (MySqlCommand command = new MySqlCommand(sql, con))
                    {
                            command.Parameters.AddWithValue("@NomFR",S);
                            using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            String s = "";
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                s += reader.GetName(i) + " ";
                            }
                            Console.WriteLine(s);
                            while (reader.Read())
                            {
                                for (int i = 0; i < reader.FieldCount; i++)
                                {
                                    var val = reader.GetValue(i);
                         
                                    Console.Write(val + " ");
                                }
                                maListe.Add(new Pays()
                                {
                                    NomFR = reader.GetFieldValue<string>(3),
                                    NomEn = reader.GetFieldValue<string>(4),

                                });
                            }

                            dataGridView2.DataSource = maListe;
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Erreur d'affichage : " + ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                con.Close(); 

            }

        }

        public void rechercheCapitaleFR(string s)
        {
            List<Pays> maListe = new List<Pays>();
            using (MySqlConnection con = new MySqlConnection(chaineConnexionPays))
            {
                con.Open();
                try
                {
                    string sql = "SELECT * FROM paysTable WHERE CapitaleFR = @CapitaleFR";

                    using (MySqlCommand command = new MySqlCommand(sql, con))
                    {
                        command.Parameters.AddWithValue("@CapitaleFR", s); 

                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                maListe.Add(new Pays()
                                {
                                    
                                    CapitaleEN= reader["CapitaleEN"].ToString(),
                                });
                            }
                        }
                    }

                    dataGridView2.DataSource = maListe;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Erreur d'affichage : " + ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        public void addPays(String codePays,String alpha2,String alpha3,String paysFR,String paysEN) {

            using (MySqlConnection con = new MySqlConnection(chaineConnexionPays))
            {
                string drapeauxFolder = Path.Combine(Application.StartupPath, "drapeaux");
                try
                {
                    con.Open();
                    using (MySqlCommand command = new MySqlCommand("INSERT INTO paysTable (CodeNum, Alpha2, Alpha3, NomFR, NomEn, CapitaleFR, CapitaleEN) VALUES (@CodeNum, @Alpha2, @Alpha3, @NomFR, @NomEn, NULL, NULL)", con))
                    {
                        command.Parameters.AddWithValue("@CodeNum", codePays);
                        command.Parameters.AddWithValue("@Alpha2", alpha2.ToUpper());
                        command.Parameters.AddWithValue("@Alpha3", alpha3.ToUpper());
                        command.Parameters.AddWithValue("@NomFR", paysFR);
                        command.Parameters.AddWithValue("@NomEn", paysEN);
                        command.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Erreur d'affichage : " + ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }


                string selectQuery = "SELECT * FROM paysTable ORDER BY  CodeNum";
                using (MySqlDataAdapter adapter = new MySqlDataAdapter(selectQuery, con))
                {
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);
                    dataGridView2.DataSource = dataTable;
                }

                con.Close();
               

            }

        }
        public void AddCapitale(string s1, string s2, string s3)
        {
            using (MySqlConnection con = new MySqlConnection(chaineConnexionPays))
            {
                string CapitaleFR;
                string CapitaleEN;
                if (s2 == null)
                { CapitaleFR = ""; }
                else { CapitaleFR = s2; }

                if (s3 == null) { CapitaleEN = ""; }
                else
                {
                    CapitaleEN = s3;
                }

                try
                {
                    con.Open();
                    string updateQuery = "UPDATE paysTable SET CapitaleFR = @CapitaleFR, CapitaleEN = @CapitaleEN WHERE Alpha2 = @Alpha2";

                    using (MySqlCommand command = new MySqlCommand(updateQuery, con))
                    {
                        command.Parameters.AddWithValue("@CapitaleFR", CapitaleFR);
                        command.Parameters.AddWithValue("@CapitaleEN", CapitaleEN);
                        command.Parameters.AddWithValue("@Alpha2", s1); 
                        command.ExecuteNonQuery();
                    }

     
                    string selectSingleQuery = "SELECT * FROM paysTable WHERE Alpha2 = @Alpha2";
                    using (MySqlCommand selectCommand = new MySqlCommand(selectSingleQuery, con))
                    {
                        selectCommand.Parameters.AddWithValue("@Alpha2", s1); 
                        using (MySqlDataAdapter adapter = new MySqlDataAdapter(selectCommand))
                        {
                            DataTable dataTable = new DataTable();
                            adapter.Fill(dataTable);
                            dataGridView2.DataSource = dataTable;
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Erreur d'affichage : " + ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }



        private void avecAlpha2ToolStripMenuItem_Click(object sender, EventArgs e)
        {   Form2 fille=new Form2(this);
            fille.Show();
            
            
        }

        private void rechercheToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form5 recherche = new Form5(this);
            recherche.Show();
        }

        private void ajouterUnPaysToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form3 capitale = new Form3(this);
            capitale.Show();
        }

        public void importerDrapeaux()
        {
            string drapeauxFolder = Path.Combine(Application.StartupPath, "drapeaux");

            using (MySqlConnection con = new MySqlConnection(chaineConnexionPays))
            {
                con.Open();

             
                string alterTableQuery = "ALTER TABLE paysTable ADD COLUMN Drapeau BLOB";
                using (MySqlCommand alterCommand = new MySqlCommand(alterTableQuery, con))
                {
                    try
                    {
                        alterCommand.ExecuteNonQuery();
                    }
                    catch (MySqlException)
                    {
                        
                    }
                }

                string[] drapeauFiles = Directory.GetFiles(drapeauxFolder);

                foreach (string drapeauFile in drapeauFiles)
                {
                    string alpha2 = Path.GetFileNameWithoutExtension(drapeauFile);

                    byte[] imageBytes = File.ReadAllBytes(drapeauFile);

                    string updateQuery = "UPDATE paysTable SET Drapeau = @Drapeau WHERE Alpha2 = @Alpha2";

                    using (MySqlCommand command = new MySqlCommand(updateQuery, con))
                    {
                        command.Parameters.AddWithValue("@Drapeau", imageBytes);
                        command.Parameters.AddWithValue("@Alpha2", alpha2);
                        command.ExecuteNonQuery();
                    }
                }
                

                string selectQuery = "SELECT * FROM paysTable ORDER BY  CodeNum";
                using (MySqlDataAdapter adapter = new MySqlDataAdapter(selectQuery, con))
                {
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);
                    dataGridView2.DataSource = dataTable; 
                }
            }
        }
       



        private void importerDrapeauxToolStripMenuItem_Click(object sender, EventArgs e)
        {
            importerDrapeaux();
        }

        private void ajouterUnPaysToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Form4 fille = new Form4(this);
            fille.Show();
           
        }
    }
    
}
