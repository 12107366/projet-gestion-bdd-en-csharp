﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MiniProjetPays
{
    public partial class Form2 : Form
    {
        public static Form1 pere;
        
        public Form2(Form1 p)
        {
            InitializeComponent();
            pere = p;
        }

        private void Form2_Load(object sender, EventArgs e)
        {
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            pere.rechercheAlpha2(textBox1.Text);
            this.Close();
        }
    }
}
