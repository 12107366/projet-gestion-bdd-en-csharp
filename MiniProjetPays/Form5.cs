﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MiniProjetPays
{
    public partial class Form5 : Form
    {
        Form1 pere;
        public Form5(Form1 p)
        {
            InitializeComponent();
            pere = p;
        }

        private void Form5_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (radioButton1.Checked) { pere.rechercheAlpha2(textBox1.Text); }
            else if (radioButton2.Checked) { pere.recherchePaysFR(textBox1.Text); }
            else if (radioButton3.Checked) { pere.rechercheCapitaleFR(textBox1.Text); }
            this.Close();
        }
    }
}
