﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MiniProjetPays
{
    public partial class Form3 : Form
    {
        Form1 pere;
        public Form3(Form1 pere)
        {
            InitializeComponent();
            this.pere = pere;
        }

        private void Form3_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            String alpha2 = textBox1.Text;
            String capFR= textBox2.Text;
            String capEN= textBox3.Text;
            pere.AddCapitale(alpha2,capFR,capEN);
            this.Close();


        }
    }
}
