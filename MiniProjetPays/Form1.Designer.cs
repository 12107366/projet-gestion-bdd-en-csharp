﻿namespace MiniProjetPays
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.afficherLesPaysToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ajouterUnPaysToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chercherUnPaysToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.avecAlpha2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rechercheToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importerDrapeauxToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ajouterUnPaysToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.pageSetupDialog1 = new System.Windows.Forms.PageSetupDialog();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(29, 62);
            this.dataGridView2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.RowHeadersWidth = 62;
            this.dataGridView2.RowTemplate.Height = 28;
            this.dataGridView2.Size = new System.Drawing.Size(1196, 407);
            this.dataGridView2.TabIndex = 2;
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1255, 28);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.afficherLesPaysToolStripMenuItem,
            this.ajouterUnPaysToolStripMenuItem,
            this.chercherUnPaysToolStripMenuItem,
            this.rechercheToolStripMenuItem,
            this.importerDrapeauxToolStripMenuItem,
            this.ajouterUnPaysToolStripMenuItem1});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(60, 24);
            this.toolStripMenuItem1.Text = "Menu";
            // 
            // afficherLesPaysToolStripMenuItem
            // 
            this.afficherLesPaysToolStripMenuItem.Name = "afficherLesPaysToolStripMenuItem";
            this.afficherLesPaysToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.afficherLesPaysToolStripMenuItem.Text = "Afficher les pays";
            this.afficherLesPaysToolStripMenuItem.Click += new System.EventHandler(this.afficherLesPaysToolStripMenuItem_Click);
            // 
            // ajouterUnPaysToolStripMenuItem
            // 
            this.ajouterUnPaysToolStripMenuItem.Name = "ajouterUnPaysToolStripMenuItem";
            this.ajouterUnPaysToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.ajouterUnPaysToolStripMenuItem.Text = "ajouter une capitale";
            this.ajouterUnPaysToolStripMenuItem.Click += new System.EventHandler(this.ajouterUnPaysToolStripMenuItem_Click);
            // 
            // chercherUnPaysToolStripMenuItem
            // 
            this.chercherUnPaysToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.avecAlpha2ToolStripMenuItem});
            this.chercherUnPaysToolStripMenuItem.Name = "chercherUnPaysToolStripMenuItem";
            this.chercherUnPaysToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.chercherUnPaysToolStripMenuItem.Text = "chercher un pays";
            // 
            // avecAlpha2ToolStripMenuItem
            // 
            this.avecAlpha2ToolStripMenuItem.Name = "avecAlpha2ToolStripMenuItem";
            this.avecAlpha2ToolStripMenuItem.Size = new System.Drawing.Size(179, 26);
            this.avecAlpha2ToolStripMenuItem.Text = "Avec Alpha 2";
            this.avecAlpha2ToolStripMenuItem.Click += new System.EventHandler(this.avecAlpha2ToolStripMenuItem_Click);
            // 
            // rechercheToolStripMenuItem
            // 
            this.rechercheToolStripMenuItem.Name = "rechercheToolStripMenuItem";
            this.rechercheToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.rechercheToolStripMenuItem.Text = "Recherche ";
            this.rechercheToolStripMenuItem.Click += new System.EventHandler(this.rechercheToolStripMenuItem_Click);
            // 
            // importerDrapeauxToolStripMenuItem
            // 
            this.importerDrapeauxToolStripMenuItem.Name = "importerDrapeauxToolStripMenuItem";
            this.importerDrapeauxToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.importerDrapeauxToolStripMenuItem.Text = "Importer drapeaux";
            this.importerDrapeauxToolStripMenuItem.Click += new System.EventHandler(this.importerDrapeauxToolStripMenuItem_Click);
            // 
            // ajouterUnPaysToolStripMenuItem1
            // 
            this.ajouterUnPaysToolStripMenuItem1.Name = "ajouterUnPaysToolStripMenuItem1";
            this.ajouterUnPaysToolStripMenuItem1.Size = new System.Drawing.Size(224, 26);
            this.ajouterUnPaysToolStripMenuItem1.Text = "Ajouter un pays";
            this.ajouterUnPaysToolStripMenuItem1.Click += new System.EventHandler(this.ajouterUnPaysToolStripMenuItem1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1255, 521);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem afficherLesPaysToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ajouterUnPaysToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem chercherUnPaysToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem avecAlpha2ToolStripMenuItem;
        private System.Windows.Forms.PageSetupDialog pageSetupDialog1;
        private System.Windows.Forms.ToolStripMenuItem rechercheToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importerDrapeauxToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ajouterUnPaysToolStripMenuItem1;
    }
}

