﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MiniProjetPays
{
    public partial class Form4 : Form
    {

        public static Form1 pere;

        public Form4(Form1 p)
        {
            InitializeComponent();
            pere = p;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            pere.addPays(textBox1.Text, textBox2.Text, textBox3.Text, textBox4.Text, textBox5.Text);
            pere.importerDrapeaux();
            Close();

        }

        private void Form4_Load(object sender, EventArgs e)
        {
            
        }
    }
}
